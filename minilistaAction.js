const LOAD_MINILISTA = 'LOAD_MINILISTA'

export default function loadMinilistaAction(payload) {
  return {
    type: LOAD_MINILISTA,
    payload
  }
}

export {LOAD_MINILISTA}
