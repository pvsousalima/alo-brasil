import {
    Alert,
    Platform,
    AsyncStorage
} from 'react-native'

import Contacts from 'react-native-contacts'
import axios from 'axios'

// axios.defaults.baseURL = 'https://minilista.herokuapp.com'
// let apiUrl = 'https://minilista.herokuapp.com'
// axios.defaults.baseURL = 'http://localhost:3000'
// let apiUrl = 'http://localhost:3000'

axios.defaults.baseURL = 'http://54.233.226.196'
// let apiUrl = 'http://54.233.226.196'


// Saves to AsyncStorage
export const cacheToAsyncStorage = (key, value) => {
    return AsyncStorage.setItem('@MySuperStore:'+key, JSON.stringify(value))
}

// Reads from AsyncStorage
export const readFromAsyncStorage = (key) => {
    return new Promise((resolve, reject) => {
        AsyncStorage.getItem('@MySuperStore:'+key).then((item) => {
            resolve(JSON.parse(item))
        })
    })
}
// Reads from AsyncStorage
export const deleteFromAsyncStorage = (key) => {
    return new Promise((resolve, reject) => {
        AsyncStorage.remove('@MySuperStore:'+key).then(() => {

        })
    })
}

// Merge with AsyncStorage
export const mergeWithAsyncStorage = (key, value) => {
    return AsyncStorage.mergeItem('@MySuperStore:'+key, JSON.stringify(value))
}

// A custom toast alert
export const customToastAlert = (title, message) => {
    Alert.alert(
        title,
        message,
        { cancelable: true }
    )
}

// mascara para telephones
export const mtel = (v) => {
    v=v.replace(/\D/g,"")             //Remove tudo o que não é dígito
    v=v.replace(/^(\d{2})(\d)/g,"($1) $2") //Coloca parênteses em volta dos dois primeiros dígitos
    v=v.replace(/(\d)(\d{4})$/,"$1-$2")    //Coloca hífen entre o quarto e o quinto dígitos
    return v
}

// Adicionar numero configuravel
export const addWhatsAppAloBrasil = () => {
    readFromAsyncStorage('contato').then((read) => {
        if(read === false){
            cacheToAsyncStorage('contato', true).then(() => {
                const ContatoAloBrasil = {
                    phoneNumbers: [{
                        label: "mobile",
                        number: "(555) 555-5555",
                    }],
                    familyName: 'Brasil Comunicação',
                    givenName: Platform.OS === 'ios' ? 'Alô' : 'Alô Brasil Comunicação',
                }
                Contacts.addContact(ContatoAloBrasil, (err) => {
                    err ? customToastAlert('Erro', 'Não foi possível adicionar o contato WhatsApp do Alô Brasil aos seus contatos! :(') : customToastAlert('Sucesso!', 'Contato WhatsApp do Alô Brasil foi adicionado aos seus contatos!')
                })
            })
        }
    })
}

export const configureOptionsAlerts = () => {
    readFromAsyncStorage('configureOptionsAlert').then((read) => {
        if(read === false){
            cacheToAsyncStorage('base', 'Araranguá-SC').then(() => {
            })
            cacheToAsyncStorage('configureOptionsAlert', true).then(() => {
                !read ? customToastAlert('Aviso', 'Você pode configurar o aplicativo com seus dados no meu ao lado!') : null
                !read ? customToastAlert('Aviso', 'Deslize a lista para baixo para atualizar sua base de dados.') : null
            })
        }
    })
}

// Recupera os contatos do usuario
export const getContacts = () => {
    return new Promise((resolve, reject) => {
        Contacts.getAll((err, contacts) => {
            if (err) {
                reject(err)
            } else {
                resolve(contacts)
            }
        })
    })
}

export const getBase = (base) => {
    return new Promise((resolve, reject) => {

        let configOptions = {
            method: 'GET',
            url: base ? '/carregaBase?base=' + base : '/carregaBase',
            headers: {
                'x-access-token': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1OTNiNWFjODczMmVlMTJjNDE0NGI1MTgiLCJ0aXR1bG8iOiJQZWRybyBMaW1hIiwicnVhIjoiQXZlbmlkYSBDb3JvbmVsIEpvw6NvIEZlcm5hbmRlcyIsIm51bWVybyI6IjE2NiIsImNvbXBsZW1lbnRvIjoiIiwiYmFpcnJvIjoiQ2VudHJvIiwiY2lkYWRlIjoiQXJhcmFuZ3XDoSIsInVmIjoiU0MiLCJjZXAiOiI4ODkwMDAwMCIsInNpdGUiOiIiLCJlbWFpbCI6IiIsImRlc2NyaWNhbyI6IiIsImZhY2Vib29rSWQiOiIxMDAxMTAxMjAzIiwiaW1hZ2VtIjpudWxsLCJhdGl2byI6dHJ1ZSwiYmFzZSI6IkFyYXJhbmd1w6EtU0MiLCJfX3YiOjAsInRlbGVmb25lcyI6W3sibnVtZXJvIjoiKDMxKSA5MjUwIDE0NjUiLCJvcGVyYWRvcmEiOiJ0aW0ifV0sImlhdCI6MTQ5NzA2MjU1MCwiZXhwIjoxNTI4NjIwMTUwfQ.f9Pbm1RBpCxq-MDty3qDgacIDUUqnq9JIOI_gnH7i6M',
                'Content-Type': 'application/json'
            }
        }

        /* post to api using the configuration */
        axios(configOptions).then((response) => {
            resolve(response.data)
        }).catch((error) => {
            console.log(error);
            reject(error)
        })
    })
}

export const carregaBaseNomes = () => {
    return new Promise((resolve, reject) => {

        let configOptions = {
            method: 'GET',
            url: '/carregaBaseNomes',
            headers: {
                'x-access-token': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1OTNiNWFjODczMmVlMTJjNDE0NGI1MTgiLCJ0aXR1bG8iOiJQZWRybyBMaW1hIiwicnVhIjoiQXZlbmlkYSBDb3JvbmVsIEpvw6NvIEZlcm5hbmRlcyIsIm51bWVybyI6IjE2NiIsImNvbXBsZW1lbnRvIjoiIiwiYmFpcnJvIjoiQ2VudHJvIiwiY2lkYWRlIjoiQXJhcmFuZ3XDoSIsInVmIjoiU0MiLCJjZXAiOiI4ODkwMDAwMCIsInNpdGUiOiIiLCJlbWFpbCI6IiIsImRlc2NyaWNhbyI6IiIsImZhY2Vib29rSWQiOiIxMDAxMTAxMjAzIiwiaW1hZ2VtIjpudWxsLCJhdGl2byI6dHJ1ZSwiYmFzZSI6IkFyYXJhbmd1w6EtU0MiLCJfX3YiOjAsInRlbGVmb25lcyI6W3sibnVtZXJvIjoiKDMxKSA5MjUwIDE0NjUiLCJvcGVyYWRvcmEiOiJ0aW0ifV0sImlhdCI6MTQ5NzA2MjU1MCwiZXhwIjoxNTI4NjIwMTUwfQ.f9Pbm1RBpCxq-MDty3qDgacIDUUqnq9JIOI_gnH7i6M',
                'Content-Type': 'application/json'
            }
        }

        /* post to api using the configuration */
        axios(configOptions).then((response) => {
            resolve(response.data)
        }).catch((error) => {
            reject(error)
        })
    })
}


export const registraUsuario = (usuario) => {
    return new Promise((resolve, reject) => {

        let configOptions = {
            method: 'POST',
            url: '/usuario',
            data: JSON.stringify(usuario),
            headers: {
                'Content-Type': 'application/json'
            }
        }

        /* post to api using the configuration */
        axios(configOptions).then((response) => {
            resolve(response.data)
        }).catch((error) => {
            reject({code: 'ERRO', mensagem: 'Email já foi cadastrado previamente!'})
        })
    })
}

// sair do aplicativo
export const logout = async () => {
    return new Promise((resolve, reject) => {
        AsyncStorage.removeItem("profile").then((remove) => {
            resolve(AsyncStorage.clear())
        })
    })
}

// verifica se ta logado
export const isLogged = () => {
    return new Promise((resolve, reject) => {
        AsyncStorage.getItem('profile').then((profile) => {
            profile ? resolve(profile) : reject(null)
        })
    })

}

// default api actions when the app starts
addWhatsAppAloBrasil()
configureOptionsAlerts()
