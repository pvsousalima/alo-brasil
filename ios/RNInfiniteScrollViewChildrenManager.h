//
//  TableViewChildren.h
//  example
//
//  Created by Tal Kol on 6/8/16.
//  Copyright © 2016 Facebook. All rights reserved.
//

#import <React/RCTViewManager.h>   // Required when used as a Pod in a Swift project

@interface RNInfiniteScrollViewChildrenManager : RCTViewManager

@end
