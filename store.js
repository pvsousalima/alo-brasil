
import {combineReducers, applyMiddleware, compose, createStore} from 'redux'
import {Provider} from 'react-redux'

import {persistStore, autoRehydrate} from 'redux-persist'
import {AsyncStorage} from 'react-native'
import logger from 'redux-logger'

import {profileReducer, historyReducer, minilistaReducer} from './reducers'

const store = createStore(
  combineReducers({
    profileReducer,
    historyReducer,
    minilistaReducer
  }),
  applyMiddleware(logger),
  autoRehydrate({log: true})
)

persistStore(store, {storage: AsyncStorage}, () => {
  console.log('rehydration complete')
})

export default Provider
export {store}
