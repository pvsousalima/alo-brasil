import React from 'react'
import PropTypes from 'react'

import {
    ActivityIndicator,
    Button,
    Text,
    View,
    ScrollView,
    Share,
    ListView,
    TouchableOpacity
} from 'react-native'

import {List, ListItem, SideMenu} from 'react-native-elements'
import {NavigationActions} from 'react-navigation'
import Icon from 'react-native-vector-icons/MaterialIcons'
import {sortBy} from 'lodash'

import {connect} from 'react-redux'
import {persistStore, autoRehydrate} from 'redux-persist'
import {AsyncStorage} from 'react-native'

import {store} from '../store'
import updateProfileAction from '../profileActions'
import loadMinilistaAction from '../minilistaAction'

import {
    readFromAsyncStorage,
    customToastAlert,
    getBase,
    logout
} from '../API'

import {
    Card,
    MyStatusBar,
    Login,
    CustomHeaderWithSearchBar,
    PhoneNumber,
    ShareButton,
    LogoutButton,
    TryAgainButton
} from './Components'

import
    phoneLogic
 from './Components/PhoneNumber'

import styles from './styles'

const logoutAction = NavigationActions.reset({
    index: 0,
    actions: [
        NavigationActions.navigate({ routeName: 'RegisterScreen'})
    ]
})

class HomeScreen extends React.Component {

    static navigationOptions = () => ({
        title: 'Minilista',
        header: null,
        tabBarIcon: ({ tintColor }) => (
            <Icon name="list" size={30} color={tintColor} />
        )
    })

    updateDataSource(newDataSource){
        this.setState({
            dataSource: this.state.dataSource.cloneWithRows(newDataSource)
        })
    }

    constructor(props){
        super(props)

        // Listview Data source
        this.ds = new ListView.DataSource({
            rowHasChanged: (r1, r2) => r1 !== r2
        })

        // state
        this.state = {
            dataSource: this.ds.cloneWithRows([]),
            data: [],
            isOpen: true,
            animating: true,
            loading: false,
        }

        // function binders
        this.updateDataSource = this.updateDataSource.bind(this)
        this.onSideMenuChange = this.onSideMenuChange.bind(this)
        this.toggleSideMenu = this.toggleSideMenu.bind(this)
        this.loadBase = this.loadBase.bind(this)
        this.renderRow = this.renderRow.bind(this)
    }

    loadBase(){

        const {base} = this.props.profile

        // check update
        this.setState({loading: true, animating: true})

            getBase(base).then((base) => {
                const filter = base.map((el) => {
                    return {
                        titulo: el.titulo.trim(),
                        telefone: el.telefone,
                        endereco: el.endereco,
                        descricao: el.descricao,
                        email: el.email,
                        base: el.base
                    }
                })
                this.setState({
                    data: sortBy(filter, 'titulo'),
                    loading:false,
                    animating: false,
                    isOpen: false
                })
                this.updateDataSource(filter)
            }).catch(() => {
                customToastAlert(
                    'Erro',
                    'Não foi possível carregar a base, cheque sua conexão etente novamente'
                )
                this.setState({animating: false, loading: false, isOpen:false})
            })

    }

    componentWillMount(){
        this.loadBase()
    }

    onSideMenuChange (isOpen: boolean) {
        this.setState({
            isOpen: isOpen
        })
    }

    toggleSideMenu () {
        this.setState({
            isOpen: !this.state.isOpen
        })
    }

    renderOptions(profile){

        let options = [
           {
               name: 'DDD',
               subtitle: profile.ddd,
               navigate: 'InsertPhoneScreen'
           },
           {
               name: 'Telefone',
               subtitle: profile.telephone,
               navigate: 'InsertPhoneScreen'
           },
           {
               name: 'Operadora',
               subtitle: profile.operator,
               navigate: 'OperatorScreen'
           },
           {
               name: 'Base de dados',
               subtitle: profile.base,
               navigate: 'DatabaseScreen'
           }
       ]

       return (
           <List>
               {
                   options.map((l, i) => (
                       <ListItem
                           roundAvatar
                           onPress={() => {
                               this.props.navigation.navigate(l.navigate, {
                                   refreshSideMenu: this.refreshSideMenu,
                                   loadBase: this.loadBase
                               })}
                           }
                           key={i}
                           title={l.name}
                           subtitle={l.subtitle}
                       />
                   ))
               }
           </List>
       )
    }

    renderRow = (data) => {
        return (
            <Card {...data} onPress={() => {
                this.props.navigation.navigate('ProfileScreen', data)
            }}/>
        )
    }

    render() {

        const {navigate} = this.props.navigation
        const {profile, minilista} = this.props

        return(
            <SideMenu
                isOpen={this.state.isOpen}
                onChange={this.onSideMenuChange.bind(this)}
                menu={(
                    <View style={styles.sideMenuContainer}>

                        <ScrollView showsVerticalScrollIndicator={false}>

                            <Login/>

                            <LogoutButton onPress={() => {
                                logout().then(() => {
                                    this.props.navigation.dispatch(logoutAction)
                                })
                            }}/>

                            <View style={styles.sideMenuOptionsContainer}>
                                <Text style={styles.optionsText}>
                                    Minhas configurações
                                </Text>
                            </View>

                            {this.renderOptions(profile)}

                            <ShareButton onPress={() => {
                                Share.share({
                                    message: 'Já ouviu falar do alô brasil?',
                                    title: 'Alô Brasil App!'
                                })
                            }}/>

                        </ScrollView>
                    </View>
                )}>

                <View key={'main'} style={styles.mainListContainer}>

                    <MyStatusBar
                        backgroundColor="#5E8D48"
                        barStyle="light-content"
                    />

                    <CustomHeaderWithSearchBar
                        toggleSideMenu={this.toggleSideMenu}
                        updateDataSource={this.updateDataSource}
                        data={this.state.data}/>

                    {
                        this.state.data.length > 0 &&
                        this.state.loading === false ?
                            <ListView
                                decelerationRate = 'normal'
                                enableEmptySections
                                removeClippedSubviews={false}
                                showsVerticalScrollIndicator={true}
                                scrollRenderAheadDistance={200}
                                dataSource={
                                    this.state.dataSource
                                }
                                renderRow={this.renderRow}/>
                        :
                        <ActivityIndicator
                            style={{marginTop: 30}}
                            animating={this.state.animating}
                        />
                    }

                    {
                        this.state.data.length === 0 && this.state.loading  === false &&
                        <TryAgainButton onPress={() => { this.loadBase()}}/>
                    }

                </View>
            </SideMenu>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        profile: state.profileReducer,
    }
}

const mapDispatchToProps = (dispatch) => {
    return {}
}

export default connect(mapStateToProps, mapDispatchToProps)(HomeScreen)
