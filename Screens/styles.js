import {
    Dimensions,
} from 'react-native'

const {width, height} = Dimensions.get('window')

const styles = {

    mainListContainer: {
        flex:1,
        height: height - 64,
        width: width,
        left: 0,
        bottom: 0,
        backgroundColor: '#F7F7F7',
        shadowOpacity: 0.3
    },

    sideMenuContainer: {
        flex: 1,
        backgroundColor: '#3b5885',
        paddingTop: 10,
        shadowOpacity:1,
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'center'
    },

    sideMenuOptionsContainer: {
        flexDirection:'column',
        marginTop: 30,
        flex: 1
    },

    optionsText: {
        marginLeft:10,
        color:'white',
        fontWeight:'500'
    },

    // the left menu
    phoneNumberStyle: {
        color:'rgba(0,0,0,0.4)',
        marginLeft: 11,
        fontSize: 12,
        fontWeight:'600'
    }
}

export default styles
