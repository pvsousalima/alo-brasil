import React from 'react'
import PropTypes from 'react'

import {
    Dimensions,
    Text,
    Image,
    ListView,
    View,
    StatusBar,
    SectionList,
} from 'react-native'

import {ContactsListCard} from './Components'
import {Button, SearchBar} from 'react-native-elements'
import Icon from 'react-native-vector-icons/MaterialIcons'
import Contacts from 'react-native-contacts'
import {groupBy, sortBy} from 'lodash'
import {readFromAsyncStorage} from '../API'

import {connect} from 'react-redux'
import updateProfileAction from '../profileActions'

const {width} = Dimensions.get('window')

const styles = {
    notFoundContainer: {
        flex:1,
        alignItems:'center',
        justifyContent:'center',
        backgroundColor:'#3b5885'
    },

    searchBarContainer: {
        borderRadius: 5,
        height: 30,
        width: width-28,
        backgroundColor:'white',
        marginLeft:15
    },

    inputBarContainer: {
        backgroundColor: 'white',
        height: 15
    },

    topProfileContainer: {
        height: 80,
        flexDirection: 'row',
        justifyContent:'flex-start',
        alignItems:'center',
        marginLeft: 10
    }
}

class AllContactsScreen extends React.PureComponent {

    static navigationOptions = () => ({
        title: 'Contatos',
        header: null,
        tabBarIcon: ({ tintColor }) => (
            <Icon name="contact-phone" size={25} color={tintColor} />
        )
    })

    constructor() {
        super()

        this.ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2})

        this.state = {
            contactsList: [],
            isLoading: true,
            clearIcon: {color: 'transparent'},
            dataSource: this.ds.cloneWithRows([]),
            profile: {}
        }
    }

    loadProfile(){
        readFromAsyncStorage('profile').then((profile) => {
            this.setState({profile: profile})
        })
    }

    componentWillMount(){
        this.getContacts()
        this.loadProfile()
    }

    updateDataSource(newDataSource){
        this.setState({
            dataSource: this.state.dataSource.cloneWithRows(newDataSource)
        })
    }

    getContacts(){
        return new Promise((resolve, reject) => {
            Contacts.getAll((err, contacts) => {
                if (err) {
                    reject(err)
                } else {

                    const sections = []
                    const extractor = groupBy(contacts, (el) => {
                        return el.givenName.substring(0,1)
                    })

                    Object.keys(extractor).map((key) => {
                        const listItems = []
                        extractor[key].map((el) => {
                            listItems.push(el)
                        })
                        sections.push({data: listItems, title: key})
                    })

                    this.updateDataSource(contacts)
                    this.setState({contactsList: sortBy(sections, ['title', 'givenName']), isLoading:false})
                }
            })
        })
    }

    keyExtractor = (item) => item.recordID;

    renderRow(item) {
        return <ContactsListCard {...item} onPress={() => {
            // this.props.navigation.navigate('ProfileScreen', item)
        }}/>
    }

    renderResults(text) {
        if(text.length > 0) {
            let newContacts = []
            this.state.contactsList.map((el) => {
                if(text !== '' && el.givenName.toLowerCase().includes(text.toLowerCase())){
                    newContacts.push(el)
                }
            })
            this.updateDataSource(newContacts)
        } else {
            this.updateDataSource(this.state.contactsList)
        }
    }

    _keyExtractor = (item) => item.recordID;

    render() {

        // const {profile} = this.state

        const {profile} = this.props

        // console.log(this.props)

        return (
            <View style={{flex:1, backgroundColor:'white'}}>

                <StatusBar barStyle="light-content"/>

                <View style={{backgroundColor: '#3b5998', paddingTop:24, paddingBottom:10}}>

                    <View style={{ alignItems:'flex-start', 'flexDirection': 'row', top:2}}>
                        {/* <TouchableOpacity style={{marginLeft:15, width:30, height:30, top:3}}>
                            <Icon name="add" size={25} style={{justifyContent:'center'}} color={'white'}/>
                            </TouchableOpacity>
                        */}
                        <SearchBar
                            lightTheme
                            containerStyle={styles.searchBarContainer}
                            inputStyle={styles.inputBarContainer}
                            icon={{color: '#86939e', name: 'search', style:{top:7} }}
                            clearIcon={this.state.clearIcon}
                            placeholder='Pesquisar nos meus contatos'
                            textInputRef='contato'
                            onFocus={() => {
                                this.setState({clearIcon: {color: '#86939e', name: 'clear', style:{top:7}}})
                            }}
                            onBlur={() => {
                                this.setState({clearIcon: {color: 'transparent', name: 'clear'}})
                            }}
                            onChangeText={(text) => { this.renderResults(text) }}
                        />
                    </View>
                </View>

                <View style={styles.topProfileContainer}>

                    <Image style={{
                        width: 40,
                        height: 40,
                        borderRadius: 20,
                    }} source={{uri: profile.picture}}/>

                    <View style={{
                        flexDirection:'column',
                        paddingLeft: 10
                    }}>
                        <Text style={{
                            fontSize: 20,
                            fontWeight:'300'
                        }}>{`${profile.first_name} ${profile.last_name}`}
                        </Text>

                        <Text style={{
                            fontWeight:'600',
                            color:'rgba(0,0,0,0.5)'
                        }}>
                            Meu número: {profile.telephone}
                        </Text>
                    </View>

                </View>

                { this.state.contactsList.length > 0 ?
                    <SectionList
                        removeClippedSubviews={false}
                        renderItem={({item}) => (
                            <View>
                                <Text style={{
                                    padding: 8,
                                    paddingBottom: 10,
                                    paddingTop: 10,
                                    zIndex: 0,
                                    fontWeight: '600',
                                    fontSize: 16
                                }}>
                                    {`${item.givenName} ${item.familyName}`}
                                </Text>
                                <View style={{
                                    marginLeft: 8,
                                    width: width,
                                    height: 1,
                                    backgroundColor: '#D3D3D3',
                                }}/>
                            </View>
                        )}
                        initialNumToRender={40}
                        renderSectionHeader={
                            ({section}) => <Text style={{
                                backgroundColor:'#D3D3D3',
                                padding: 8,
                                fontWeight: '600',
                                zIndex: 999999,
                                fontSize: 16
                            }}>
                                {section.title}
                            </Text>
                        }
                        keyExtractor={this._keyExtractor}
                        sections={this.state.contactsList}
                    />
                :
                <View style={styles.notFoundContainer}>
                    <Text style={{ color:'white', fontWeight:'600'}}>
                        Não encontrou nos seus contatos?
                    </Text>
                    <Button
                        textStyle={{color: '#3b5885'}}
                        buttonStyle={{backgroundColor:'white', margin:30, borderRadius:4}}
                        icon={{color: '#3b5885', name: 'search', type: 'MaterialIcons'}}
                        title='Pesquise na minilista!'
                        onPress={() => {
                            this.props.navigation.navigate("Home")
                        }}
                    />
                </View>
                }
            </View>
        )
    }

}

const mapStateToProps = (state) => {
    return {
        profile: state.profileReducer,
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        changeProfile: (profile) => {
            dispatch(updateProfileAction({profile: profile}))
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(AllContactsScreen)
