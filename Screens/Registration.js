
import React from 'react'
import {
    Dimensions,
    Image,
    Text,
    TextInput,
    View,
    Keyboard,
    StatusBar,
    TouchableOpacity,
    KeyboardAvoidingView,
    Picker,
    ScrollView,
    Switch
} from 'react-native'

import ReactNative from 'react-native'
import {NavigationActions} from 'react-navigation'
import {Button} from 'react-native-elements'
import KeyboardSpacer from 'react-native-keyboard-spacer'

import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scrollview'

import {mtel, customToastAlert, registraUsuario, cacheToAsyncStorage, readFromAsyncStorage} from '../API'
import {Card, MyStatusBar, LoginWithoutPhoto, RegisterButton, Profile} from './Components'
import Icon from 'react-native-vector-icons/MaterialIcons'

var DismissKeyboard = require('dismissKeyboard');

const {width, height} = Dimensions.get('window')

const backAction = NavigationActions.back({
    key: null
})

const styles = {

    logoImage:{
        width: 250,
        height: 250,
        marginBottom: 5
    },

    inputStyle: {
        textAlign: 'center',
        textAlignVertical: 'center',
        fontWeight: 'bold',
        marginTop: 15,
        height: 32,
        marginRight: 20,
        marginLeft:20,
        fontSize: 16,
        fontWeight: '300',
        backgroundColor: 'white',
        borderRadius: 4
    },

    inputState: {
        marginTop: 15,
        height: 40,
        marginRight: 20,
        marginLeft:20,
        backgroundColor: 'white',
        borderRadius: 4
    },

    inputStateText: {
        textAlign:'center',
        textAlignVertical: 'center',
        fontWeight: 'bold',
        marginTop: 10,
        fontSize: 16,
        fontWeight: '300',
        backgroundColor: 'white',
        borderRadius: 4
    },

    screenContainer: {
        flex: 1,
        backgroundColor:'#172666',
        justifyContent:'center',
        alignItems:'center'
    },

    adText: {
        color:'white',
        padding:10,
        fontWeight:'300',
        fontSize:14,
        textAlign:'center'
    },

    pickerContainer: {
        position:'absolute',
        justifyContent:'center',
        width: width,
        bottom: 0,
        backgroundColor:'white',
    }
}


class Registration extends React.Component {

    static navigationOptions = ({ navigation, screenProps }) => ({
        header: null,
    })

    constructor(){
        super()
    }

    state = {
        showPicker: false,


        // titulo: "Pedro Lima",
        // cidade: "Araranguá",
        // uf: "SC",
        // cep: "88900000",
        // site: "http://google.com",
        // email: "dada@gmail.coma",
        // password: "123456",
        // telefone: "31 992501465",
        // telefones: [
        //     {
        //         numero: "(31) 9250 1465",
        //         operadora: "tim"
        //     }
        // ],
        // descricao: "minha descricao",
        // imagem: null,
        // ativo: true,
        // base: "Araranguá-SC",
        // senha: "123123123",
        // estado: "SC"

    }




    // Scroll a component into view. Just pass the component ref string.
    inputFocused (refName) {
        setTimeout(() => {
            let scrollResponder = this.refs.scrollView.getScrollResponder()
            scrollResponder.scrollResponderScrollNativeHandleToKeyboard(
                ReactNative.findNodeHandle(this.refs[refName]),
                110, //additionalOffset
                true
            )
        }, 10)
    }



    render() {

        const {navigate} = this.props.navigation

        return(
            <View style={styles.screenContainer}>
                <StatusBar barStyle="light-content"/>

                <View style={{width:width, height:40, marginTop:24, flexDirection:'row'}}>
                    <TouchableOpacity
                        style={{flexDirection:'row'}}
                        onPress={() => {
                            this.props.navigation.dispatch(backAction)
                        }}>
                        <Icon name="chevron-left" size={40} color={'white'} />
                        <Text style={{margin:11, marginLeft:-5, fontWeight:'300', fontSize:16, color:'white'}}>
                            Voltar
                        </Text>
                    </TouchableOpacity>
                </View>


                <View style={{flex:1, width:width, flexDirection:'column'}}>

                    <ScrollView ref='scrollView' style={{flex:1, width: width, marginBottom:8}}>

                        <Text style={styles.adText}>
                            Antes de ter acesso as bases de dados é necessário um cadastro rápido. Preencha seus dados, faça parte
                            da nossa base de contatos e acesse as bases de números!
                        </Text>

                        <TextInput
                            ref='nome'
                            onFocus={this.inputFocused.bind(this, 'nome')}
                            keyboardType={'default'}
                            placeholder="entre com seu nome"
                            style={styles.inputStyle}
                            onChangeText={(nome) => this.setState({titulo:(nome)})}
                            value={this.state.titulo}/>

                        <TextInput
                            ref='email'
                            onFocus={this.inputFocused.bind(this, 'email')}
                            keyboardType={'email-address'}
                            placeholder="entre com seu email"
                            style={styles.inputStyle}
                            onChangeText={(email) => this.setState({email:(email)})}
                            value={this.state.email}/>

                        <TextInput
                            ref='senha'
                            secureTextEntry={true}
                            onFocus={this.inputFocused.bind(this, 'senha')}
                            keyboardType={'default'}
                            placeholder="defina uma senha para a sua conta"
                            style={styles.inputStyle}
                            onChangeText={(senha) => this.setState({senha:(senha)})}
                            value={this.state.senha}/>

                        <TextInput
                            ref='telefone'
                            onFocus={this.inputFocused.bind(this, 'telefone')}
                            keyboardType={'numeric'}
                            placeholder="digite o seu telefone"
                            style={styles.inputStyle}
                            onChangeText={(telefone) => this.setState({telefone:(mtel(telefone))})}
                            value={this.state.telefone}/>

                        <TextInput
                            ref='cidade'
                            onFocus={this.inputFocused.bind(this, 'cidade')}
                            keyboardType={'default'}
                            placeholder="digite a sua cidade"
                            style={styles.inputStyle}
                            onChangeText={(cidade) => this.setState({cidade: cidade})}
                            value={this.state.cidade}/>

                        <TextInput
                            ref='estado'
                            maxLength={2}
                            onFocus={this.inputFocused.bind(this, 'estado')}
                            keyboardType={'default'}
                            placeholder="digite seu estado"
                            style={styles.inputStyle}
                            onChangeText={(estado) => this.setState({estado: estado.toUpperCase()})}
                            value={this.state.estado}/>

                        <Text style={{marginLeft:20, marginTop:8, color:'white'}}>Sexo:</Text>

                        <View style={{flex:1, marginLeft:20, marginTop:8, flexDirection:'row'}}>
                            <Switch
                                onValueChange={(value) => this.setState({masculino: value, feminino:false})}
                                value={this.state.masculino}/>
                            <Text style={styles.adText}>
                                Masculino
                            </Text>
                            <Switch
                                onValueChange={(value) => this.setState({feminino: value, masculino:false})}
                                value={this.state.feminino}/>
                            <Text style={styles.adText}>
                                Feminino
                            </Text>
                        </View>

                        {/* <TextInput
                            ref='estado'
                            onFocus={() => this.setState({showPicker: true})}
                            onBlur={() => this.setState({showPicker:false})}
                            keyboardType={'default'}
                            placeholder="escolha seu estado"
                            style={styles.inputState}>
                        </TextInput> */}

                        {/* <KeyboardSpacer/> */}
                    </ScrollView>

                    <Button
                        textStyle={{color: '#3b5885'}}
                        buttonStyle={{backgroundColor:'white', borderRadius:4, bottom:15}}
                        icon={{color: '#3b5885', name: 'check', type: 'MaterialIcons'}}
                        title='Cadastrar'
                        onPress={() => {
                            registraUsuario(this.state).then((usuario) => {
                                cacheToAsyncStorage('profile', usuario).then((save) => {
                                    this.props.navigation.navigate("Home", usuario)
                                })
                            }).catch((erro) => {
                                customToastAlert('Erro', erro.mensagem)
                            })
                        }}/>


                    {/* {this.state.showPicker && (
                        <View style={styles.pickerContainer}>
                            <Button
                        title="Confirmar"
                        style={{marginRight: 0}}
                        onPress={() => this.setState({showPicker: false})}
                            />
                            <Picker
                        selectedValue={this.state.estado}
                        onValueChange={(itemValue, itemIndex) =>
                        this.setState({estado: itemValue})}>
                        <Picker.Item label="Java" value="java" />
                        <Picker.Item label="JavaScript" value="js"/>
                            </Picker>
                        </View>
                    )} */}
                </View>
            </View>
        )
    }
}


export default Registration
