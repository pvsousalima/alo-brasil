
import React from 'react'

import {
    Alert,
    Dimensions,
    StatusBar,
    Button,
    Image,
    Text,
    TouchableOpacity,
    View
} from 'react-native'

import {
    SearchBar,

} from 'react-native-elements'

import {customToastAlert, cacheToAsyncStorage, readFromAsyncStorage} from '../../API'
import Icon from 'react-native-vector-icons/MaterialIcons'
import FBSDK from 'react-native-fbsdk'

const {
    AccessToken,
    LoginManager,
    GraphRequest,
    GraphRequestManager,
} = FBSDK

let {width} = Dimensions.get('window')

import { NavigationActions } from 'react-navigation'

import {connect} from 'react-redux'
import updateProfileAction from '../../profileActions'

import {FacebookButton} from './'

const resetAction = NavigationActions.reset({
    index: 0,
    actions: [
        NavigationActions.navigate({ routeName: 'Home'})
    ]
})

class LoginWithoutPhoto extends React.Component {

    constructor(){
        super()
        this.facebookLogin = this.facebookLogin.bind(this)
    }
    // Start the graph request.
    facebookLogin(){
        AccessToken.getCurrentAccessToken().then((data) => {
            new GraphRequestManager().addRequest(
                new GraphRequest(
                    '/me',
                    {
                        accessToken: data.accessToken,
                        parameters: {
                            fields: {
                                string: 'email,name,first_name,middle_name,last_name'
                            }
                        }
                    },
                    (error: ?Object, result: ?Object) => {
                        if (error) {
                            customToastAlert('Erro', 'Erro ao carregar informacões: ' + error.toString())
                        } else {

                            let user = result

                            user['picture'] = "https://graph.facebook.com/"+user.id+"/picture?height=180"

                            this.props.changeProfile({
                                first_name:  user.first_name,
                                id: user.id,
                                last_name: user.last_name,
                                name: user.name,
                                picture: user.picture,
                                logged: true
                            })

                            cacheToAsyncStorage('logged', true).then(() => {
                                // this.props.foo.navigate("Home", {type: 'BACK'})
                                this.props.foo.dispatch(resetAction)
                            })
                        }
                    },
                )
            ).start()
        })
    }

    render () {
        return (
            <FacebookButton facebookLogin={this.facebookLogin}/>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        profile: state.profileReducer,
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        changeProfile: (profile) => {
            dispatch(updateProfileAction({profile: profile}))
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(LoginWithoutPhoto)
