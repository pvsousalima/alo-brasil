import React from 'react'
import PropTypes from 'react'

import {
    Text
} from 'react-native'

import {readFromAsyncStorage} from '../../API'

class PhoneNumber extends React.Component {

    render(){

        const {telephone, style} = this.props

        return (
            <Text style={style}>{telephone}</Text>
        )
    }
}

export {PhoneNumber}
