import React, {Component, PureComponent} from 'react'
import {
    AppRegistry,
    Text,
    Dimensions,
    View,
    Button,
    Platform,
    TouchableHighlight,
    Image
} from 'react-native';

import Icon from 'react-native-vector-icons/MaterialIcons'

const styles = {
    Touchable: {
        padding: 5,
        flex: 1,
        backgroundColor: 'white',
        borderRadius: 4,
        shadowColor: 'black',
        shadowOffset: {width: 0, height: 1},
        // shadowOpacity: 0.05,
        // shadowRadius: 0.5,
        // borderWidth: 1,
        // borderColor: 'rgba(0, 0, 0, 0.06)'
    }
}

class ContactsListCard extends Component {

    componentDidMount(){
        // console.log(this.props)
    }

    render() {

        let {width, height} = Dimensions.get('window')
        const {familyName, givenName} = this.props

        return (

            <View>

                <TouchableHighlight
                    activeOpacity={0.70}
                    underlayColor={'white'}
                    onPress={this.props.onPress}
                    style={styles.Touchable}>

                    <View style={{
                        flexDirection:'row',
                        flex:1,
                        padding:8
                    }}>

                        {/* <Image
                            style={{width: 35, height: 35, borderRadius: 17}}
                        source={require('../Images/tim.png')}/> */}

                        <Text
                            numberOfLines={1}
                            style={{
                                    marginLeft: 10,
                                    marginTop: 4,
                                    fontSize: 14,
                                    color: '#ABABAB',
                                fontWeight: !familyName ? 'bold' : null
                            }}>
                            { `${givenName} `}
                            <Text
                                numberOfLines={1}
                                style={{
                                        marginLeft: 0,
                                        fontSize: 14,
                                        color: '#ABABAB',
                                        fontWeight: 'bold',
                                }}>
                                { `${familyName} `}
                            </Text>
                        </Text>

                    </View>


                </TouchableHighlight>

                <View style={{
                    backgroundColor:'rgba(0,0,0,0.05)',
                    marginTop: 2,
                    height: 1,
                    width: width,
                    flexDirection:'row-reverse'
                }}/>

            </View>
        )
    }
}


export default ContactsListCard;
