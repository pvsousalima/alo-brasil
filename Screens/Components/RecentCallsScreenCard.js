import React, {PureComponent} from 'react'
import {
    AppRegistry,
    Text,
    Dimensions,
    View,
    Button,
    Platform,
    TouchableHighlight,
    Image
} from 'react-native';

import {mtel} from '../../API/api.js'
import Icon from 'react-native-vector-icons/MaterialIcons'

class RecentCallsScrenCard extends React.PureComponent {

    render() {

        let {width, height} = Dimensions.get('window')

        return (
            <View>
                <View style={{
                    padding: 5,
                }}>
                    <TouchableHighlight
                        activeOpacity={0.70}
                        underlayColor={'white'}
                        onPress={this.props.onPress}
                        style={{
                            padding: 5,
                            flex: 1,
                            backgroundColor: 'white',
                            borderRadius: 4,
                            shadowColor: 'black',
                            shadowOffset: {width: 0, height: 1},

                        }}>

                        <View style={{height: 40, flexDirection: 'row'}}>

                            <View style={{
                                flex:1,
                                height: 40,
                                justifyContent: 'center',
                                alignItems: 'flex-start',
                            }}>

                                <Text
                                    numberOfLines={1}
                                    style={{
                                        marginLeft: 0,
                                        fontSize: 16,
                                        color:'#3b5998',
                                        fontWeight: !this.props.familyName ? 'bold' : null
                                    }}>
                                    { `${mtel(this.props.numberCalled)} `}
                                </Text>

                                <Text numberOfLines={2} style={{
                                    fontSize: 12,
                                    fontWeight: 'bold',
                                    color: '#ABABAB',
                                    top: 5
                                }}>
                                    { `${new Date(this.props.date).getHours()}:${new Date(this.props.date).getUTCMinutes()}` }
                                </Text>
                            </View>

            </View>

        </TouchableHighlight>
    </View>
    <View style={{backgroundColor:'#ccc', top:2, height:0.4, width: width}}></View>

</View>
)
}
}


export default RecentCallsScrenCard;
