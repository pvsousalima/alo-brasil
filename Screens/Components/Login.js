

import React from 'react'

import {
    Alert,
    Dimensions,
    StatusBar,
    Button,
    Image,
    Text,
    TouchableOpacity,
    View
} from 'react-native'

import {
    SearchBar,

} from 'react-native-elements'

import {customToastAlert, cacheToAsyncStorage, readFromAsyncStorage} from '../../API'
import Icon from 'react-native-vector-icons/MaterialIcons'
import FBSDK from 'react-native-fbsdk'

const {
    AccessToken,
    LoginManager,
    GraphRequest,
    GraphRequestManager,
} = FBSDK

let {width} = Dimensions.get('window')

import { NavigationActions } from 'react-navigation'

import {connect} from 'react-redux'
import updateProfileAction from '../../profileActions'

import {FacebookButton} from './'

const resetAction = NavigationActions.reset({
    index: 0,
    actions: [
        NavigationActions.navigate({ routeName: 'Home'})
    ]
})

export class Login extends React.Component {

    constructor(props) {
        super(props)
    }

    renderProfile(){

        const {profile} = this.props

        return (
            <View style={{alignItems:'center',marginTop:10}}>
                {profile.picture && <Image
                    style={{width: 90, height: 90, borderRadius:45, shadowOpacity:5}}
                    source={{uri: profile.picture}}/>
                }
                <Text style={{
                    margin:10,
                    color:'white',
                    fontSize: 22,
                    fontWeight:'600',
                    marginBottom:15
                }}>{`${profile.name || profile.titulo}`}</Text>
            </View>
        )
    }

    render() {
        return (
            <View style={{justifyContent:'center', alignItems:'center'}}>
                {this.renderProfile()}
            </View>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        profile: state.profileReducer,
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        changeProfile: (profile) => {
            dispatch(updateProfileAction({profile: profile}))
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Login)
