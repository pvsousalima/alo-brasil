import React, {PureComponent} from 'react'
import {
    AppRegistry,
    Text,
    View,
    Button,
    Platform,
    TouchableHighlight,
    Image
} from 'react-native'

import {mtel} from '../../API'

const style = {
    container: {
        backgroundColor:'white',
        borderRadius: 5,
        margin: 10,
        shadowOpacity: 0.2,
        shadowOffset: {x: 0, y: 5},
        shadowRadius: 2,
        marginBottom: 0,
        padding: 15,
    },

    titulo: {
        fontSize: 24,
        fontWeight: '600'
    },

    telefone: {
        fontSize: 20,
        fontWeight: '300',
        color: '#3b5998'
    },

    endereco: {
        fontSize: 16,
        fontWeight: '300',
        color: '#333',
        marginTop: 10
    },

    descricao: {
        fontSize: 14,
        fontWeight: '300',
        color: '#333',
        marginTop:10
    }

}


class Card extends PureComponent {

    render() {
        const {titulo, telefone, endereco, descricao, onPress} = this.props

        return(
            <TouchableHighlight
                underlayColor={'white'}
                activeOpacity={0.3}
                style={style.container}
                onPress={onPress}>
                <View>
                    {titulo && <Text style={style.titulo}>{titulo}</Text>}
                    {telefone && telefone.length>0 ? <Text style={style.telefone}>{mtel(telefone)}</Text> : null}
                    {endereco && endereco.length>0 ? <Text style={style.endereco}>{`${endereco}`}</Text> : null}
                    {descricao && descricao.length>0 ? <Text style={style.descricao}>{`${descricao}`}</Text> : null}
                </View>
            </TouchableHighlight>
        )
    }
}


export default Card
