import React from 'react'

import {
    Alert,
    Dimensions,
    StatusBar,
    Button,
    Image,
    Text,
    TouchableOpacity,
    View
} from 'react-native'

import {
    SearchBar,

} from 'react-native-elements'

import {customToastAlert, cacheToAsyncStorage, readFromAsyncStorage} from '../../API'
import Icon from 'react-native-vector-icons/MaterialIcons'
import FBSDK from 'react-native-fbsdk'

const {
    AccessToken,
    LoginManager,
    GraphRequest,
    GraphRequestManager,
} = FBSDK

let {width} = Dimensions.get('window')

import { NavigationActions } from 'react-navigation'

import {connect} from 'react-redux'
import updateProfileAction from '../../profileActions'

const resetAction = NavigationActions.reset({
    index: 0,
    actions: [
        NavigationActions.navigate({ routeName: 'Home'})
    ]
})

export class FacebookButton extends React.Component {
    render() {
        return (
            <TouchableOpacity
                style={{borderRadius:4, backgroundColor:'#3b5998', width:200, height:40, flexDirection:'row'}}
                onPress={() => {
                    LoginManager.logInWithReadPermissions(['public_profile']).then((result) => {
                        if (result.isCancelled) {
                            Alert.alert(
                                'Atenção',
                                "Login cancelado!",
                            )
                        } else {
                            this.props.facebookLogin()
                        }
                    }, () => {
                        Alert.alert(
                            'Atenção',
                            "Erro no login!",
                        )
                    })
                }}>
                <Image style={{padding:20, width:35, height:35}} source={require('../Images/fb_icon.png')}/>
                <Text style={{paddingTop:13, paddingLeft:5, fontSize:14, fontWeight:'300', color:'white'}}>Login com Facebook</Text>
            </TouchableOpacity>
        )
    }
}

export class RegisterButton extends React.Component {
    render() {
        return (
            <TouchableOpacity
                style={{margin:this.props.margin, borderRadius:4, backgroundColor:'#329438', width:200, height:40, flexDirection:'row'}}
                onPress={() => {
                    this.props.foo.navigate("Registration")
                }}>
                <Icon name="person-add" size={35} style={{marginLeft:5, padding:3, color:'white'}} source={require('../Images/fb_icon.png')}/>
                <Text style={{paddingTop:12, paddingLeft:20, fontSize:14, fontWeight:'300', color:'white'}}>Cadastre-se!</Text>
            </TouchableOpacity>
        )
    }
}

export const MyStatusBar = ({backgroundColor, ...props}) => (
    <StatusBar backgroundColor={backgroundColor} {...props} />
)

export const ShareButton = ({onPress}) => (
    <View style={{flex:1,  margin: 8, flexDirection:'row', justifyContent:'center'}}>
        <TouchableOpacity onPress={onPress}>
            <View style={{flexDirection:'row', height: 40}}>
                <Icon name="share" size={20} color={'white'}/>
                <Text style={{color:'white', marginTop:1, fontSize:14, fontWeight:'400'}}>
                    Compartilhar o app!
                </Text>
            </View>
        </TouchableOpacity>
    </View>
)

export const LogoutButton = ({onPress}) => (
    <TouchableOpacity onPress={onPress}>
        <View style={{flexDirection:'row', marginLeft:30}}>
            <Icon name="exit-to-app" size={20} color={'white'}/>
            <Text style={{
                color:'white',
                marginTop:0,
                fontSize:16,
                fontWeight:'400'
            }}>Sair do aplicativo</Text>
        </View>
    </TouchableOpacity>
)

export const TryAgainButton = ({onPress}) => (
    <View style={{flex:1, justifyContent:'center', alignItems:'center'}}>
        <Text style={{fontWeight:'300', marginBottom:20, textAlign:'center'}}>
            Nenhum resultado carregado! Selecione outra base ou tente novamente.
        </Text>
        <Button title={'Tentar novamente'} onPress={onPress}/>
    </View>
)


const styles = {
    barContainer : {
        backgroundColor: '#3b5998', paddingTop:24, paddingBottom:10
    },

    leftIcon: {
        marginLeft:15, width:30, height:30, top:3
    },

    rightIcon: {
        top:3, marginLeft:10
    },

    containerStyle: {
        borderRadius: 5, height: 30, width: width-70, backgroundColor:'white', marginRight:10, marginLeft:15
    },

    inputStyle: {
        backgroundColor: 'white', height: 15
    },

    icon: {
        color: '#86939e', name: 'search', style:{top:7}
    }
}

export class CustomHeaderWithSearchBar extends React.Component{

    constructor() {
        super()
        this.state= {
            clearIcon: {color: 'transparent'}
        }
    }

    fetchNewDataSourceFromSearch = (data, text) => {
        if(data){
            if(text.length > 0) {
                let newDataSource = []
                data.map((el) => {
                    if(text !== '' && el.titulo.toLowerCase().includes(text.toLowerCase())){
                        newDataSource.push(el)
                    }
                })
                return newDataSource
            }
        }
    }

    render() {

        const {data} = this.props

        return (

            <View style={styles.barContainer}>

                <View style={{alignItems:'flex-start', 'flexDirection': 'row', top:2}}>

                    <TouchableOpacity
                        style={styles.leftIcon}
                        onPress={() => {this.props.toggleSideMenu()}}>
                        <Icon name="menu"
                            size={25}
                            style={{justifyContent:'center'}}
                            color={'white'}/>
                    </TouchableOpacity>

                    <SearchBar
                        lightTheme
                        containerStyle={styles.containerStyle}
                        inputStyle={styles.inputStyle}
                        icon={styles.icon}
                        clearIcon={this.state.clearIcon}
                        placeholder='Pesquisar na minilista'
                        textInputRef='minilista'
                        onFocus={() => {
                            this.setState({clearIcon: {color: '#86939e', name: 'clear', style:{top:7}}})
                        }}
                        onBlur={() => {
                            this.setState({clearIcon: {color: 'transparent', name: 'clear'}})
                        }}
                        onChangeText={(text) => {
                            if(data){
                                if(text.length > 0){
                                    this.props.updateDataSource(this.fetchNewDataSourceFromSearch(data, text))
                                } else {
                                    this.props.updateDataSource(data)
                                }
                            }
                        }}
                    />

                </View>
            </View>
        )
    }
}
