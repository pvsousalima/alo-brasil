
import React from 'react';
import {
    ActivityIndicator,
    Image,
    Text,
    TextInput,
    View,
    StatusBar
} from 'react-native';

import {NavigationActions} from 'react-navigation';
import {Button} from 'react-native-elements'
import KeyboardSpacer from 'react-native-keyboard-spacer';

import {mtel, readFromAsyncStorage, cacheToAsyncStorage, isLogged, logout} from '../API'
import {Card, MyStatusBar, LoginWithoutPhoto, RegisterButton, Profile} from './Components'
import * as Animatable from 'react-native-animatable';

import {connect} from 'react-redux'
import updateProfileAction from '../profileActions'

const resetAction = NavigationActions.reset({
    index: 0,
    actions: [
        NavigationActions.navigate({ routeName: 'Home'})
    ]
})

const styles = {
    animatable: {
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 5,
        paddingTop: 20,
        padding: 10,
        flexDirection:'column',
    },

    logoImage:{
        width: 250,
        height: 250,
        marginBottom: 5
    },

    screenContainer: {
        flex: 1,
        backgroundColor:'#172666',
        justifyContent:'center',
        alignItems:'center'
    },

    adText: {
        textAlign:'center',
        color:'white',
        fontWeight:'300'
    }
}


class RegisterScreen extends React.Component {

    static navigationOptions = ({ navigation, screenProps }) => ({
        header: null,
    })

    state = {
        logged: false,
        loading: true
    }

    constructor(){
        super()
    }

    componentWillMount(){

        const {profile} = this.props
        // console.log(this.props);
        profile ?
        this.setState({logged: true, profile: profile, loading:false}) :
        this.setState({logged: false, loading:false})
    }

    SignIn = () => {
        return (
            <View style={styles.screenContainer}>
                <StatusBar barStyle="light-content"/>
                <Animatable.View
                    animation="fadeIn"
                    duration={2000}
                    style={styles.animatable}>
                    <Image
                        style={styles.logoImage}
                        source={require('./Images/icon.png')}/>
                    </Animatable.View>
                    <LoginWithoutPhoto foo={this.props.navigation}/>
                    <RegisterButton margin={10} foo={this.props.navigation}/>
                    <View style={{marginTop:30}}>
                        <Text style={styles.adText}>
                            Entre agora e acesse bases telefônicas de
                            cidades brasileiras!
                        </Text>
                    </View>
                </View>
            )
        }

        SignedIn = () => {
            return (
                <View>
                    {this.props.navigation.dispatch(resetAction)}
                </View>
            )
        }

    render() {
        const {navigate} = this.props.navigation
        const {profile} = this.props

        if(!profile.logged && this.state.loading){
            return (
                <View style={styles.screenContainer}>
                    <ActivityIndicator style={{top:20}} animating={true}/>
                </View>
            )
        } else {
            if(!this.state.loading && !profile.logged){
                return this.SignIn()
            }

            if(profile.logged && !this.state.loading){
                return this.SignedIn()
            }

            return <View/>
        }

    }
}

const mapStateToProps = (state) => {
    return {
        profile: state.profileReducer,
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        changeProfile: (profile) => {
            dispatch(updateProfileAction({profile: profile}))
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(RegisterScreen)
