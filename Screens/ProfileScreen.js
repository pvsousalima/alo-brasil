import React from 'react';
import {
    Animated,
    AppRegistry,
    Dimensions,
    Text,
    View,
    Button,
    Image,
    RefreshControl,
    ScrollView,
    AsyncStorage,
    StatusBar,
    ListView,
    TouchableOpacity,
    TextInput
} from 'react-native';

import {StackNavigator} from 'react-navigation';
import {List, ListItem, SearchBar, Divider} from 'react-native-elements'
import {Card} from './Components'
import faker from 'faker'
import Icon from 'react-native-vector-icons/MaterialIcons'
import {AutoGrowingTextInput} from 'react-native-autogrow-textinput';
import * as Animatable from 'react-native-animatable';

import {mtel} from '../API'

const {width, height} = Dimensions.get('window')

class EditableWidget extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            editMode: true
        }
    }

    renderAction() {
        if (this.state.editMode) {
            return <AutoGrowingTextInput {...this.props}
                style={{flex: 1, marginLeft: 20, marginTop:10, marginBottom:10, height: 40, fontSize: 16, color: 'rgb(14,122,254)'}}
                placeHolder={this.props.actionTitle}/>
            }
        }

        render() {
            return (
                <TouchableOpacity>
                    <Text style={{color: 'black', fontSize:16, marginLeft: 20, top:5, bottom:5}}>{this.props.actionTitle}</Text>
                    {this.renderAction()}
                </TouchableOpacity>

            )
        }
    }

    export class Separator extends React.Component {
        render() {
            return (
                <View style={{
                    width: this.props.width,
                    backgroundColor: 'rgba(9,9,9, 0.05)',
                    height: 1,
                    alignSelf: 'flex-end'
                }}/>
            )
        }
    }

    class Group extends React.Component {
        render() {
            return (
                <View
                    style={{
                        backgroundColor: 'rgba(500, 500, 500, 0.06)',
                        marginTop: 5,
                        marginBottom: 5,
                    }}
                    {...this.props}
                >
                    { this.props.children }
                </View>
                )
            }
        }

        class ProfileScreen extends React.Component {

            static navigationOptions = {
                headerStyle: {
                    backgroundColor: '#3b5998',
                },
                headerTintColor:'white'
            }

            constructor(props){
                super(props)
                this.state = {}
            }

            componentDidMount(){
                // fazer a busca de anunciantes aqui
            }


            render() {
                const {navigate} = this.props.navigation
                this.props = this.props.navigation.state.params

                return(
                    <View key={'main'} style={{flex:1, height: height - 64, width: width, left: 0, bottom: 0, backgroundColor: '#F7F7F7'}}>

                        <View style={{backgroundColor:'rgba(0,0,0,0.01)', flex: 1}}>

                            <ScrollView showsVerticalScrollIndicator={true}>

                                <View style={{flex:1}}>

                                    <Animatable.View
                                        animation="fadeIn"
                                        duration={2000}
                                        style={{
                                            justifyContent: 'center',
                                            alignItems: 'center',
                                            marginTop: 5,
                                            paddingTop: 20,
                                            padding: 10,
                                            flexDirection:'column',
                                        }}>

                                        {this.props.image && <Image
                                            style={{width: 90, height: 90, borderRadius: 45, margin: 10}}
                                            source={{uri: this.props.imagem}}/>
                                        }

                                        <Text numberOfLines={2} style={{
                                            marginLeft: 10,
                                            fontWeight: '300',
                                            fontSize: 26,
                                            color: 'black'
                                        }}> {this.props.titulo}  </Text>

                                    </Animatable.View>

                                    <Group style={{backgroundColor:'white', marginTop: 30}}>
                                        <Separator width={width - 50}/>
                                        <EditableWidget
                                            editable={false}
                                            actionTitle={'mobile'}
                                            value={mtel(this.props.telefone)}/>
                                        <Separator width={width - 50}/>
                                        <EditableWidget
                                            editable={false}
                                            actionTitle={'endereço'}
                                            value={this.props.endereco}/>
                                        <Separator width={width - 50}/>
                                        <EditableWidget
                                            editable={false}
                                            actionTitle={'website'}
                                            value={this.props.site}/>
                                        <Separator width={width - 50}/>
                                        <EditableWidget
                                            editable={false}
                                            actionTitle={'email'}
                                            value={this.props.email}/>
                                        <Separator width={width - 50}/>
                                        {/* <EditableWidget
                                            editable={false}
                                            actionTitle={'base de dados'}
                                        value={this.props.base}/> */}
                                    </Group>



                                </View>
                            </ScrollView>

                            {/* <Animatable.View  animation="fadeIn" duration={1000} style={{backgroundColor:'white', alignItems:'center'}}>
                                <Animatable.Text >
                                    <Text numberOfLines={2} style={{
                                justifyContent:'center',
                                fontWeight: '300',
                                fontSize: 14,
                                color: '#3b5885',
                                    }}> Outros procuraram por {this.props.nome}  </Text>
                                </Animatable.Text>
                            </Animatable.View> */}

                                </View>
                            </View>
                        )
                    }
                }


                export default ProfileScreen;
