
import React from 'react';
import {
    Text,
    View,
    ActivityIndicator,
    ListView
} from 'react-native';
import {NavigationActions} from 'react-navigation';

import {ListItem } from 'react-native-elements'

import {carregaBaseNomes, cacheToAsyncStorage} from '../API'

import {connect} from 'react-redux'
import updateProfileAction from '../profileActions'

const backAction = NavigationActions.back({
    key: null
})

class DatabaseScreen extends React.Component {

    static navigationOptions = () => ({
        header: null,
    })

    constructor(){
        super()
        this.ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
        this.state = {
            checked:false,
            dataSource: this.ds.cloneWithRows([])
        }
    }

    componentWillMount(){
        carregaBaseNomes().then((nomes) => {
            this.setState({dataSource: this.ds.cloneWithRows(nomes)})
        })
    }

    render() {
        const {navigation: {state: {params}}} = this.props
        return(
            <View style={{flex: 1, backgroundColor:'#3b5998', justifyContent:'center'}}>

                <View style={{flex:1, margin:10, marginTop:37}}>

                    <View style={{justifyContent:'center', alignItems:'center', margin: 10}}>

                        <Text style={{ color:'white', fontWeight:'600'}}>
                            Selecione a sua base telefônica favorita!
                        </Text>

                    </View>
                    {
                        (<ListView
                            decelerationRate = 'normal'
                            enableEmptySections
                            showsVerticalScrollIndicator={false}
                            dataSource={this.state.dataSource}
                            renderRow={(data) =>
                                <ListItem
                                    roundAvatar
                                    title={data}
                                    containerStyle={{backgroundColor:'white'}}
                                    onPress={() => {
                                        this.props.changeProfile({
                                            base: data
                                        })
                                        params.loadBase()
                                        this.props.navigation.dispatch(backAction)
                                    }}
                                />
                            }
                         />
                        ) || (<View style={{flex:1, justifyContent:'center',alignItems:'center'}}>
                            <ActivityIndicator/><Text >carregando</Text>
                        </View>)
                    }
                </View>

            </View>
        )
    }
}



const mapStateToProps = (state) => {
    return {
        profile: state.profileReducer,
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        changeProfile: (profile) => {
            dispatch(updateProfileAction({profile: profile}))
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(DatabaseScreen)
