
import React from 'react';
import {
    AppRegistry,
    Dimensions,
    Text,
    View,
    RefreshControl,
    ActivityIndicator,
    AsyncStorage,
    ScrollView,
    Platform,
    ListView
} from 'react-native';
import { StackNavigator, NavigationActions } from 'react-navigation';

import {Card} from './Components';
import faker from 'faker';
import Icon from 'react-native-vector-icons/MaterialIcons';
import { List, Button, ListItem } from 'react-native-elements'
import {readFromAsyncStorage, cacheToAsyncStorage} from '../API'

import {connect} from 'react-redux'
import updateProfileAction from '../profileActions'

const list = [
    {
        name: 'TIM',
        image: require('./Images/tim.png')
    },
    {
        name: 'Vivo',
        image: require('./Images/vivo.png')
    },
    {
        name: 'Oi',
        image: require('./Images/oi.png')
    },
    {
        name: 'Claro',
        image: require('./Images/claro.png')
    }
]

const backAction = NavigationActions.back({
  key: null
})

class OperatorScreen extends React.Component {

    static navigationOptions = ({ navigation, screenProps }) => ({
        header: null,
    })

    constructor(){
        super()
        this.ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});

        this.state = {
            dataSource: this.ds.cloneWithRows(list)
        }
    }

    render() {
        return(
            <View style={{
                flex: 1,
                backgroundColor:'#3b5998',
                justifyContent:'center'
            }}>

                <View style={{
                    flex:1,
                    margin:10,
                    marginTop:37
                }}>

                    <View style={{
                        justifyContent:'center',
                        alignItems:'center',
                        margin: 10
                    }}>
                        <Text style={{
                            color:'white',
                            fontWeight:'600'
                        }}>
                            Selecione a sua operadora telefônica!
                        </Text>
                    </View>
                    {true ?
                        (<ListView
                            decelerationRate = 'normal'
                            enableEmptySections
                            showsVerticalScrollIndicator={false}
                            dataSource={this.state.dataSource}
                            renderRow={(data) =>
                                <ListItem
                                    roundAvatar
                                    title={data.name}
                                    avatar={data.image}
                                    subtitle={data.subtitle}
                                    containerStyle={{backgroundColor:'white'}}
                                    onPress={() => {
                                        this.props.changeProfile({
                                            operator: data.name
                                        })
                                        this.props.navigation.dispatch(backAction)
                                    }}
                                />
                            }
                         />) : (<View style={{flex:1, justifyContent:'center',alignItems:'center'}}>
                            <ActivityIndicator/><Text >carregando</Text>
                        </View>)
                    }
                </View>
            </View>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        profile: state.profileReducer,
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        changeProfile: (profile) => {
            dispatch(updateProfileAction({profile: profile}))
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(OperatorScreen)
