import React, {PureComponent} from 'react'
import {
    AsyncStorage,
    ActivityIndicator,
    Alert,
    Button,
    Dimensions,
    AppRegistry,
    Text,
    View,
    ListView,
    RefreshControl,
    Platform,
    StatusBar,
    TouchableOpacity,
    ScrollView
} from 'react-native'

import {readFromAsyncStorage, cacheToAsyncStorage} from '../API'
import {RecentCallsScreenCard} from './Components'
import Communications from 'react-native-communications'
import Icon from 'react-native-vector-icons/MaterialIcons'
var shortid = require('shortid')

import {connect} from 'react-redux'
import addHistoryAction from '../historyActions'
import clearHistoryAction from '../clearHistoryAction'


let {width, height} = Dimensions.get('window')

const CustomHeader = ({onPress}) => (
    <View style={{backgroundColor: '#3b5998', paddingTop:24, paddingBottom:10}}>
        <View style={{ alignItems:'flex-start', 'flexDirection': 'row', top:2}}>
            <TouchableOpacity style={{marginLeft:15, width:60, height:30, top:5}} onPress={onPress}>
                <Text style={{color:'white', fontSize:17}}>Limpar</Text>
            </TouchableOpacity>
        </View>
    </View>
)

const EmptyCallsComponent = () => (
    <View style={{
        flex:1,
        justifyContent:'center',
        alignItems:'center'
    }}>
        <Text style={{fontWeight:'600'}}>
            Não há chamadas anteriores
        </Text>
    </View>
)

class RecentCallsScren extends React.PureComponent {

    static navigationOptions = {
        header: null,
        title: 'Histórico',
        tabBarIcon: ({ tintColor }) => (
            <Icon name="history" size={26} color={tintColor} />
        ),
    }

    constructor() {
        super()

        this.ds = new ListView.DataSource({
            rowHasChanged: (r1, r2) => r1 !== r2
        })

        this.state = {
            dataSource: this.ds.cloneWithRows([])
        }
    }

    render() {

        const {history, clearHistory} = this.props

        return (
            <View style={{
                flex:1,
                backgroundColor:'white',
                flexDirection:'column'
            }}>

                <CustomHeader
                    clearHistory={this.clearHistory}
                    onPress={() => {
                        clearHistory()
                    }}
                />
                {
                    history.length > 0 ?
                        (
                            <ListView
                                decelerationRate = 'normal'
                                enableEmptySections
                                showsVerticalScrollIndicator={true}
                                removeClippedSubviews={false}
                                dataSource={this.ds.cloneWithRows(history)}
                                renderRow={(data) =>
                                    <RecentCallsScreenCard
                                        key={shortid.generate()} {...data}
                                        onPress={() => {
                                            Communications.phonecall(
                                                data.numberCalled, false
                                            )
                                        }}/>
                                }
                            />
                        )
                    :
                    (
                        <EmptyCallsComponent/>
                    )
                }
            </View>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        history: state.historyReducer.history,
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        clearHistory: () => {
            dispatch(clearHistoryAction())
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(RecentCallsScren)
