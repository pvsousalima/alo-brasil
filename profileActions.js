const UPDATE_PROFILE = 'UPDATE_PROFILE'

export default function updateProfileAction(payload) {
  return {
    type: UPDATE_PROFILE,
    payload
  }
}

export {UPDATE_PROFILE}
