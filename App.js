import {AppRegistry} from 'react-native'
import React, {Component} from 'react'
import {StackNavigator, TabNavigator} from 'react-navigation'

import Provider, {store} from './store'

import {
    HomeScreen,
    DialerScreen,
    AllContactsScreen,
    ProfileScreen,
    DatabaseScreen,
    InsertPhoneScreen,
    OperatorScreen,
    RecentCallsScreen,
    RegisterScreen,
    Registration
} from './Screens'

const MainScreenNavigator = TabNavigator({
    Home: {
        screen: HomeScreen
    },
    Recents: {
        screen: RecentCallsScreen
    },
    Discador: {
        screen: DialerScreen
    },
    AllContactsScreen: {
        screen: AllContactsScreen
    }
},
{
    initialRouteName: 'Home',
    tabBarOptions: {
        labelStyle: {
            fontSize: 12
        },
        pressOpacity:0,
        style: {
            backgroundColor: 'white'
        },
        activeTintColor: '#3b5998',
        inactiveTintColor: '#ccc',
        indicatorStyle: {
            backgroundColor:'#3b5998'
        },
        showIcon: true
    },
})

const MoboApp = StackNavigator({
    DatabaseScreen: { screen: DatabaseScreen },
    InsertPhoneScreen: { screen: InsertPhoneScreen },
    Home: { screen: MainScreenNavigator },
    ProfileScreen:{ screen: ProfileScreen },
    OperatorScreen:{ screen: OperatorScreen },
    RegisterScreen: {screen: RegisterScreen},
    Registration: {screen: Registration}
},
// the stack navigator props, transitions, etc
{
    initialRouteName: 'RegisterScreen',
    headerMode: 'screen',
    cardStyle: {
        opacity: 1,
        shadowOpacity: 0.2,
    },
}
)

const App  = () => (
    <Provider store={store}>
        <MoboApp/>
    </Provider>
)

AppRegistry.registerComponent('NavigationTest', () => App )
