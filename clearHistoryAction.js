const CLEAR_HISTORY = 'CLEAR_HISTORY'

export default function clearHistoryAction(payload) {
  return {
    type: CLEAR_HISTORY,
    payload
  }
}

export {CLEAR_HISTORY}
