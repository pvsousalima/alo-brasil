const ADD_HISTORY = 'ADD_HISTORY'

export default function addHistoryAction(payload) {
  return {
    type: ADD_HISTORY,
    payload
  }
}

export {ADD_HISTORY}
