import {REHYDRATE} from 'redux-persist/constants'
import {UPDATE_PROFILE} from './profileActions'
import {ADD_HISTORY} from './historyActions'
import {CLEAR_HISTORY} from './clearHistoryAction'
import {LOAD_MINILISTA} from './minilistaAction'

function profileReducer(state = {}, action) {
  switch (action.type) {
    case REHYDRATE:
      var incoming = action.payload.myReducer
      if (incoming) return {...state, ...incoming, specialKey: processSpecial(incoming.specialKey)}
      return state
    case UPDATE_PROFILE:
        // retrive stored data for reducer callApi
        return {...state, ...action.payload.profile}
        break
    default:
      return state
      break
  }
}

const initialHistoryState = {
    history:[]
}

function historyReducer(state = initialHistoryState, action) {
  switch (action.type) {
    case ADD_HISTORY:
        return {
            ...state,
            history: state.history.concat(action.payload.newHistory)
            // history: state.history.concat(action.newHistory)
        }
        break
    case CLEAR_HISTORY:
        return {
            ...state,
            history: []
        }
    default:
      return state
      break
  }
}


const initialMinilistaState = {
    minilista:[]
}

function minilistaReducer(state = initialMinilistaState, action) {
  switch (action.type) {
    case LOAD_MINILISTA:
        return {
            ...state,
            minilista: action.payload
        }
        break
    default:
      return state
      break
  }
}


export {profileReducer, historyReducer, minilistaReducer}
